Number of matches player per year
2009 ------ 57
2008 ------ 58
2017 ------ 59
2016 ------ 60
2015 ------ 59
2014 ------ 60
2013 ------ 76
2012 ------ 74
2011 ------ 73
2010 ------ 60





Team wise wins per year
Delhi Daredevils-2013 ------ 3
Delhi Daredevils-2014 ------ 2
Delhi Daredevils-2011 ------ 4
Delhi Daredevils-2012 ------ 11
Pune Warriors-2013 ------ 4
Delhi Daredevils-2010 ------ 7
Pune Warriors-2011 ------ 4
Rising Pune Supergiants-2016 ------ 5
Pune Warriors-2012 ------ 4
Chennai Super Kings-2009 ------ 8
Chennai Super Kings-2008 ------ 9
Deccan Chargers-2009 ------ 9
Kolkata Knight Riders-2012 ------ 12
Deccan Chargers-2008 ------ 2
Kolkata Knight Riders-2011 ------ 8
Kolkata Knight Riders-2014 ------ 11
Kolkata Knight Riders-2013 ------ 6
Kolkata Knight Riders-2010 ------ 7
Royal Challengers Bangalore-2008 ------ 4
Rajasthan Royals-2008 ------ 13
Royal Challengers Bangalore-2009 ------ 9
Rajasthan Royals-2009 ------ 6
-2011 ------ 1
Delhi Daredevils-2017 ------ 6
Mumbai Indians-2009 ------ 5
Kolkata Knight Riders-2016 ------ 8
Mumbai Indians-2008 ------ 7
Kolkata Knight Riders-2015 ------ 7
Delhi Daredevils-2015 ------ 5
-2015 ------ 2
Kolkata Knight Riders-2017 ------ 9
Delhi Daredevils-2016 ------ 7
Mumbai Indians-2016 ------ 7
Mumbai Indians-2015 ------ 10
Mumbai Indians-2014 ------ 7
Mumbai Indians-2013 ------ 13
Mumbai Indians-2012 ------ 10
Mumbai Indians-2011 ------ 10
Mumbai Indians-2010 ------ 11
Sunrisers Hyderabad-2017 ------ 8
Royal Challengers Bangalore-2010 ------ 8
Royal Challengers Bangalore-2011 ------ 10
Sunrisers Hyderabad-2016 ------ 11
Royal Challengers Bangalore-2012 ------ 8
Sunrisers Hyderabad-2015 ------ 7
Royal Challengers Bangalore-2013 ------ 9
Sunrisers Hyderabad-2014 ------ 6
Sunrisers Hyderabad-2013 ------ 10
Royal Challengers Bangalore-2014 ------ 5
Royal Challengers Bangalore-2015 ------ 8
Royal Challengers Bangalore-2016 ------ 9
Royal Challengers Bangalore-2017 ------ 3
Kolkata Knight Riders-2009 ------ 3
Deccan Chargers-2012 ------ 4
Kolkata Knight Riders-2008 ------ 6
Deccan Chargers-2011 ------ 6
Delhi Daredevils-2008 ------ 7
Delhi Daredevils-2009 ------ 10
Deccan Chargers-2010 ------ 8
Mumbai Indians-2017 ------ 12
Gujarat Lions-2016 ------ 9
Gujarat Lions-2017 ------ 4
Kings XI Punjab-2009 ------ 7
Rising Pune Supergiant-2017 ------ 10
Kings XI Punjab-2008 ------ 10
Kochi Tuskers Kerala-2011 ------ 6
Kings XI Punjab-2012 ------ 8
Kings XI Punjab-2011 ------ 7
Kings XI Punjab-2010 ------ 4
Rajasthan Royals-2010 ------ 6
Rajasthan Royals-2011 ------ 6
Rajasthan Royals-2012 ------ 7
Rajasthan Royals-2013 ------ 11
Kings XI Punjab-2016 ------ 4
Kings XI Punjab-2015 ------ 3
Kings XI Punjab-2014 ------ 12
Kings XI Punjab-2013 ------ 8
Chennai Super Kings-2015 ------ 10
Kings XI Punjab-2017 ------ 7
Chennai Super Kings-2010 ------ 9
Chennai Super Kings-2014 ------ 10
Chennai Super Kings-2013 ------ 12
Chennai Super Kings-2012 ------ 10
Chennai Super Kings-2011 ------ 11
Rajasthan Royals-2014 ------ 7
Rajasthan Royals-2015 ------ 7





Extra runs conceded by each team in the year 2016
Gujarat Lions ------ 98
Mumbai Indians ------ 102
Sunrisers Hyderabad ------ 107
Kings XI Punjab ------ 100
Delhi Daredevils ------ 106
Rising Pune Supergiants ------ 108
Kolkata Knight Riders ------ 122
Royal Challengers Bangalore ------ 156





Bowlers with best economy in the year 2015
RN ten Doeschate ------ 4.0
J Yadav ------ 4.142857142857143
R Ashwin ------ 5.871794871794871
V Kohli ------ 6.0
S Nadeem ------ 6.142857142857143
Parvez Rasool ------ 6.2
Z Khan ------ 6.36
MC Henriques ------ 6.56
M Vijay ------ 7.0
GB Hogg ------ 7.0476190476190474



highest score in any match in all times is: 188


