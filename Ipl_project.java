import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class Ipl_project {
    public static void main(String[] args) {
        String csvFile = "matches.csv";
        String line = "";
        String cvsSplitBy = ",";
        String csvFile2 = "deliveries.csv";
        String line2 = "";


        List<String> headers=new ArrayList<>();//columns of matches.csv
        List<List<String>> matrix =new ArrayList<>(10);//content of matches.csv
        List<String> headersDelilveries=new ArrayList<>();//columns of deliveries.csv
        List<List<String>> matrixDeliveries =new ArrayList<>(10);//content of deliveries.csv

        Map<String,Integer> noOfMatchesPlayedPerYear=new HashMap<>();

        Map<String, Integer> teamWinYearWise=new HashMap<>();

        List<Integer> list2016=new ArrayList<>();//match ids of matches of the year 2016
        List<Integer> list2015=new ArrayList<>();//match ids of matches of the year 2015

        Map<String,Integer> extraRuns2016=new HashMap<>();
        int overs2015=0;//total overs in 2015

        Map<String, Integer> bowlerRuns=new HashMap<>();
        Map<String, Integer> bowlerOvers=new HashMap<>();
        Map<String, Double> BowlersEconomy=new HashMap<>();

        //Highest scorer match of all times
        Map<String, Integer> matchInningScore=new HashMap<>();

        try {
            BufferedReader br = new BufferedReader(new FileReader(csvFile));
            BufferedReader br2 = new BufferedReader(new FileReader(csvFile2));
            int counter=0;
            int counter2=0;
            while ((line = br.readLine()) != null)
            {
//                String[] iPL = line.split(cvsSplitBy);
                if(counter<1)
                {
                    headers=Arrays.asList(line.split(","));
                    counter++;
                }
                else
                {
                    List<String> temp=new ArrayList<>();
                    temp=Arrays.asList(line.split(","));
                    matrix.add(temp);
                }
            }

            while ((line = br2.readLine()) != null)
            {
                if(counter2<1)
                {
                    headersDelilveries=Arrays.asList(line.split(","));
                    counter2++;
                }
                else
                {
                    List<String> temp=new ArrayList<>();
                    temp=Arrays.asList(line.split(","));
                    matrixDeliveries.add(temp);
                }
            }

            //Number of matches played per year of all the years in IPL
            int seasonIndex=headers.indexOf("season");
            for(int i=0;i<matrix.size();i++)
            {
                if(!noOfMatchesPlayedPerYear.containsKey(matrix.get(i).get(seasonIndex)))
                {
                    noOfMatchesPlayedPerYear.put(matrix.get(i).get(seasonIndex),1);
                }
                else
                {
                    noOfMatchesPlayedPerYear.put(matrix.get(i).get(seasonIndex),noOfMatchesPlayedPerYear.get(matrix.get(i).get(seasonIndex))+1);
                }
            }


            //Number of matches won of all teams over all the years of IPL.
            int winnerIndex=headers.indexOf("winner");
            for(int i=0;i<matrix.size();i++)
            {
                if(!teamWinYearWise.containsKey(matrix.get(i).get(winnerIndex)+"-"+matrix.get(i).get(seasonIndex)))
                {
                    teamWinYearWise.put(matrix.get(i).get(winnerIndex)+"-"+matrix.get(i).get(seasonIndex),1);
                }
                else
                {
                    teamWinYearWise.put(matrix.get(i).get(winnerIndex)+"-"+matrix.get(i).get(seasonIndex),teamWinYearWise.get(matrix.get(i).get(winnerIndex)+"-"+matrix.get(i).get(seasonIndex))+1);
                }
            }

            // list of all the match ids, of 2016 and 2015
            for(int i=0 ;i<matrix.size();i++)
            {
                if(matrix.get(i).get(seasonIndex).equals("2016"))
                {
                    list2016.add(Integer.parseInt(matrix.get(i).get(0)));
                }
                if(matrix.get(i).get(seasonIndex).equals("2015"))
                {
                    list2015.add(Integer.parseInt(matrix.get(i).get(0)));
                }
            }

            //list of all the extra runs conceded per team over 2016
            int extraRunIndex= headersDelilveries.indexOf("extra_runs");
            int teamNameIndex=headersDelilveries.indexOf("bowling_team");
            for(int i=0;i<matrixDeliveries.size();i++)
            {
                if(list2016.contains(Integer.parseInt(matrixDeliveries.get(i).get(0))))//checking if id is in list2016
                {
                    if(!extraRuns2016.containsKey(matrixDeliveries.get(i).get(teamNameIndex)))
                    {
                        extraRuns2016.put(matrixDeliveries.get(i).get(teamNameIndex),Integer.parseInt(matrixDeliveries.get(i).get(extraRunIndex)));
                    }
                    else
                    {
                        extraRuns2016.put(matrixDeliveries.get(i).get(teamNameIndex),extraRuns2016.get(matrixDeliveries.get(i).get(teamNameIndex)) + Integer.parseInt(matrixDeliveries.get(i).get(extraRunIndex)));
                    }
                }
            }

            //top economical bowler for 2015
            int runIndex= headersDelilveries.indexOf("total_runs");
//            System.out.println(run_index);
            for(int i=0;i<matrixDeliveries.size();i++)
            {
                if(list2015.contains(Integer.parseInt(matrixDeliveries.get(i).get(0))))//checking if id is in list2016
                {
                    int j=i;
                    int temp=i;
                    String bowlerName=matrixDeliveries.get(i).get(8);

                    while(matrixDeliveries.get(j).get(4).equals(matrixDeliveries.get(i).get(4)))
                    {
                        if(!bowlerRuns.containsKey(bowlerName))
                        {
                            bowlerRuns.put(bowlerName, Integer.parseInt(matrixDeliveries.get(j).get(runIndex)));
                        }
                        else
                        {
                            bowlerRuns.put(bowlerName,bowlerRuns.get(bowlerName)+ Integer.parseInt(matrixDeliveries.get(j).get(runIndex)));
                        }
                        j++;
                    }


                    if(!bowlerOvers.containsKey(bowlerName))
                    {
                        bowlerOvers.put(bowlerName, 1);
                    }
                    else
                    {
                        bowlerOvers.put(bowlerName, bowlerOvers.get(bowlerName) + 1);
                    }


                    i=j-1;
                    if(i!=temp)
                        overs2015++;
                }
            }

            for(int i=0;i<matrixDeliveries.size();i++)
            {
                if(!matchInningScore.containsKey(matrixDeliveries.get(i).get(0)+"-"+matrixDeliveries.get(i).get(1)))
                {
                    matchInningScore.put(matrixDeliveries.get(i).get(0)+"-"+matrixDeliveries.get(i).get(0),Integer.parseInt(matrixDeliveries.get(i).get(17)));
                }
                else
                {
                    matchInningScore.put(matrixDeliveries.get(i).get(0)+"-"+matrixDeliveries.get(i).get(0),matchInningScore.get(matrixDeliveries.get(i).get(0)+"-"+matrixDeliveries.get(i).get(1))+Integer.parseInt(matrixDeliveries.get(i).get(17)));
                }
            }



        }
        catch(IOException e)
        {
            System.out.println(e.getMessage());
        }
//        System.out.println(no_of_matches_played_per_year);//number of matches played per year

        //Number of matches player per year
        System.out.println("Number of matches player per year");
        for(Map.Entry<String, Integer> entry : noOfMatchesPlayedPerYear.entrySet())
        {
            System.out.println(entry.getKey()+" ------ "+entry.getValue());
        }

        System.out.println("\n\n\n\n");
//        System.out.println(team_win_year_wise);
        System.out.println("Team wise wins per year");
//        System.out.println("Team and Year             Number of matches won");
        for(Map.Entry<String, Integer> entry : teamWinYearWise.entrySet())
        {
            System.out.println(entry.getKey()+" ------ "+entry.getValue());
        }
//        System.out.println(list2016);
        //Extra runs conceded per team in 2016
//        System.out.println(extra_runs_2016);
        System.out.println("\n\n\n\n");
        System.out.println("Extra runs conceded by each team in the year 2016");
        for(Map.Entry<String, Integer> entry: extraRuns2016.entrySet())
        {
            System.out.println(entry.getKey()+" ------ "+entry.getValue());
        }
//        for (Map.Entry<String,Integer> entry : bowler_runs.entrySet())
//            System.out.println("Bowler = " + entry.getKey() +
//                    ", runs = " + entry.getValue());
//
//
//        for (Map.Entry<String,Integer> entry : bowler_overs.entrySet())
//            System.out.println("Bowler = " + entry.getKey() +
//                    ", overs = " + entry.getValue());
//        System.out.println(bowler_runs.size()+" "+bowler_overs.size());

        System.out.println("\n\n\n\n");
        System.out.println("Bowlers with best economy in the year 2015");
        for (Map.Entry<String,Integer> entry : bowlerOvers.entrySet())// putting economy agains the bowler i.e. economy----runs/overs
        {
            BowlersEconomy.put(entry.getKey(),(double)bowlerRuns.get(entry.getKey())/entry.getValue());
        }
//        System.out.println(BowlersEconomy);
        ArrayList<Double> sortedList=new ArrayList<>();
        for (Map.Entry<String,Double> entry : BowlersEconomy.entrySet())
        {
            sortedList.add(BowlersEconomy.get(entry.getKey()));
        }
        Collections.sort(sortedList);
//        System.out.println("List of most economical bowlers");//it can be reduced to any number 10 or 20 by just replacing .sizze() by Integral number
        for(int i=0;i<10;i++)
        {
            for(Map.Entry<String,Double> entry: BowlersEconomy.entrySet())
            {
                if(sortedList.get(i).equals(entry.getValue()))
                {
                    System.out.println(entry.getKey()+" ------ "+entry.getValue());
                }
            }
        }

        int maxScore=0;
        for(Map.Entry<String,Integer> entry: matchInningScore.entrySet())
        {
            if(maxScore<entry.getValue())
            {
                maxScore=entry.getValue();
            }
        }
        System.out.println("\n\n\n");
        System.out.println("highest score in any match in all times is: "+maxScore);

    }
}